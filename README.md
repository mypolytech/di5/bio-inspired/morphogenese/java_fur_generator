# Morphogénèse de Turing

Ce projet a pour but de générer des pelages d'animaux grâce à la méthode de Turing.


## Objectifs

L'objectif est de générer des pelages d'animaux grâce à un algorithme génétique. Il est possible de créer plusieurs générations qui sont générées suivants plusieurs modes.

## Architecture du projet

Le programme a été développé en Java.

Pour utiliser le programme il suffit de lancer le fichier "java_fur_generator.jar".

## Approche

Nous avons suivi l'algorithme proposé dans le papier fourni.

## Génération d'un individu

Afin de générer un pelage, nous avons créer une classe Person permettant de créer un individu et d'y stocker les informations.

### Attributs

Les attributs sont ceux proposés dans le papier, à savoir :

- Le taux de diffusion
- Le taux de réaction de l'activateur
- La vitesse de réaction de l'activateur
- Le taux de réaction de l'inhibiteur
- La vitesse de réaction de l'inhibiteur
- Le taux de résorption
- Le seuil d'activation
- Le nombre maximum d'itérations

Nous avons aussi ajouté trois couleurs :

- Une couleur de fond
- Une couleur intermédiaire
- Une couleur principale

#### Valeurs des attributs

Chacun des attributs précédents a une valeur aléatoire lors de la création d'un nouvel individu. La valeur de chaque paramètre est bornée afin d'obtenir des résultats intéressants dès la première génération.

## Nouvelles générations

Lors du lancement d'une nouvelle génération, plusieurs choix sont possibles :

- **Ne garder aucun parent** : dans ce cas, de nouveaux individus sont créés aléatoirement
- **Garder un seul** parent : les enfants seront des mutations du parent
- **Garder plusieurs parents** : deux parents sont sélectionnés aléatoirement dans la liste des parents. L'enfants qu'ils engendreront sera soit un croisement de ces deux parents, soit une mutation de l'un d'entre eux. *A noter qu'il s'agit d'un tirage avec remise*.

### Mutation

Pour la mutation, nous appliquons un ratio chacun des attributs du parent. Ce ratio est tiré aléatoirement pour chacun des attributs de façon à obtenir une valeur comprise entre 90% et 110% de la valeur d'origine.

*A noter que le parent reste inchangé.*

### Croisement

Un tirage au sort se fait entre les trois possibilités :

- Croisement des deux parents
- Mutation du parent 1
- Mutation du parent 2

*Nous pouvons constater que cela introduit un biais : 1 enfant sur 3 seulement est un croisement. Ce tirage aléatoire doit être revu pour rendre plus équitable le tirage au sort.*

Dans le cas d'un croisement, nous prenons aléatoirement les attributs des parents. C'est-à-dire que pour chaque attributs nous tirons à pile ou face le parent 1 ou le parent 2.

*A noter que les parents restent inchangés.*

## Utilisation du programme

La première génération est automatique.

Pour générer une nouvelle génération sélectionnez aucun, un ou plusieurs individus et cliquer sur "*nouvelle génération*".

Il est possible d'enregistrer les images en les selectionnant et en cliquant sur "Menu -> Enregistrer" ou en faisant `Ctrl + E` ou en faisant un clique droit sur l'image.
Les images seront enregistrés sous format `jpg` ou `png` au choix de l'utilisateur, et chaque image à une fichier associé au format `json` contenant les paramètres de l'image.
