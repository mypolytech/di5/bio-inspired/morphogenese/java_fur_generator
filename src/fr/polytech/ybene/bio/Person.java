package fr.polytech.ybene.bio;

import java.awt.image.BufferedImage;
import java.util.Random;

public class Person {

	// === Attributes ===
	private Random rnd = new Random();

	private float diffusionRatio;

	private float activatorReactionRatio;
	private int activatorDiffusionSpeed;

	private float inhibitorReactionRatio;
	private int inhibitorDiffusionSpeed;

	private float resorptionRatio;

	private int activationThreshold;

	private int maxIterations;

	private String personName;

	private Patch[][] patches;

	private int height;
	private int width;
	private BufferedImage buffImage;
	private int position;

	private int[] backgroundColor = new int[3];
	private int[] middleColor = new int[3];
	private int[] foregroundColor = new int[3];
	// ==================

	// === Constructor ===
	public Person(String name, int height, int width) {
		this.personName = name;
		this.height = height;
		this.width = width;
		generatePerson();

		// Création des patches
		patches = new Patch[this.height][this.width];

		for (int i = 0; i < height; ++i) {
			for (int j = 0; j < width; ++j) {
				patches[i][j] = new Patch(i, j, height, width);
			}
		}
	}
	// ===================

	// === Getters ===
	public float getDiffusionRatio() {
		return diffusionRatio;
	}

	public float getActivatorReactionRatio() {
		return activatorReactionRatio;
	}

	public int getActivatorDiffusionSpeed() {
		return activatorDiffusionSpeed;
	}

	public float getInhibitorReactionRatio() {
		return inhibitorReactionRatio;
	}

	public int getInhibitorDiffusionSpeed() {
		return inhibitorDiffusionSpeed;
	}

	public float getResorptionRatio() {
		return resorptionRatio;
	}

	public int getActivationThreshold() {
		return activationThreshold;
	}

	public int getMaxIterations() {
		return maxIterations;
	}

	public String getPersonName() {
		return personName;
	}

	public Patch[][] getPatches() {
		return patches;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public BufferedImage getBuffImage() {
		return buffImage;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public int[] getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(int[] backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public int[] getMiddleColor() {
		return middleColor;
	}

	public void setMiddleColor(int[] middleColor) {
		this.middleColor = middleColor;
	}

	public int[] getForegroundColor() {
		return foregroundColor;
	}

	public void setForegroundColor(int[] foregroundColor) {
		this.foregroundColor = foregroundColor;
	}
	// ===============

	// == Setters ==
	public void setDiffusionRatio(float diffusionRatio) {
		this.diffusionRatio = diffusionRatio;
	}

	public void setActivatorReactionRatio(float activatorReactionRatio) {
		this.activatorReactionRatio = activatorReactionRatio;
	}

	public void setActivatorDiffusionSpeed(int activatorDiffusionSpeed) {
		this.activatorDiffusionSpeed = activatorDiffusionSpeed;
	}

	public void setInhibitorReactionRatio(float inhibitorReactionRatio) {
		this.inhibitorReactionRatio = inhibitorReactionRatio;
	}

	public void setInhibitorDiffusionSpeed(int inhibitorDiffusionSpeed) {
		this.inhibitorDiffusionSpeed = inhibitorDiffusionSpeed;
	}

	public void setResorptionRatio(float resorptionRatio) {
		this.resorptionRatio = resorptionRatio;
	}

	public void setActivationThreshold(int activationThreshold) {
		this.activationThreshold = activationThreshold;
	}

	public void setMaxIterations(int maxIterations) {
		this.maxIterations = maxIterations;
	}

	public void setPatches(Patch[][] patches) {
		this.patches = patches;
	}
	// =============

	// === Methods ===
	private void generatePerson() {
		this.diffusionRatio = rnd.nextFloat();

		this.activatorReactionRatio = rnd.nextFloat();
		this.activatorDiffusionSpeed = rnd.nextInt(100) + 1;

		this.inhibitorReactionRatio = rnd.nextFloat();
		this.inhibitorDiffusionSpeed = rnd.nextInt(100) + 1;

		this.resorptionRatio = rnd.nextFloat();

		this.activationThreshold = rnd.nextInt(30) + 1;

		this.maxIterations = rnd.nextInt(10) + 1;

		for (int i = 0; i < 3; ++i) {
			this.backgroundColor[i] = rnd.nextInt(256);
			this.middleColor[i] = rnd.nextInt(256);
			this.foregroundColor[i] = rnd.nextInt(256);
		}
	}

	public void createFur() {
		// === ROUTINE ===
		for (int iterations = 0; iterations < maxIterations; ++iterations) {
			for (int i = 0; i < height; ++i) {
				for (int j = 0; j < width; ++j) {
					patches[i][j].react(activatorReactionRatio, inhibitorReactionRatio);
				}
			}

			// diffuse
			for (int i = 0; i < height; ++i) {
				for (int j = 0; j < width; ++j) {
					patches[i][j].diffuse(patches, diffusionRatio, activatorDiffusionSpeed, inhibitorDiffusionSpeed);
				}
			}

			// resorb
			for (int i = 0; i < height; ++i) {
				for (int j = 0; j < width; ++j) {
					patches[i][j].resorb(resorptionRatio);
				}
			}
		}
		// ===============

		// Création de l'image
		buffImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		int bColor = backgroundColor[0] << 16 | backgroundColor[1] << 8 | backgroundColor[2]; // Malleus malificarum !
		int mColor = middleColor[0] << 16 | middleColor[1] << 8 | middleColor[2]; // Malleus malificarum !
		int fColor = foregroundColor[0] << 16 | foregroundColor[1] << 8 | foregroundColor[2]; // Malleus malificarum !

		for (int i = 0; i < height; ++i) {
			for (int j = 0; j < width; ++j) {
				// Récupération de la valeur de l'activateur
				double activator = patches[i][j].getActivator();

				if (activator * 3.0 > activationThreshold && activator < activationThreshold) {
					buffImage.setRGB(i, j, mColor);
				} else if (activator > activationThreshold) {
					buffImage.setRGB(i, j, fColor);
				} else {
					buffImage.setRGB(i, j, bColor);
				}
			}
		}
	}

	public void recolor() {
		int bColor = backgroundColor[0] << 16 | backgroundColor[1] << 8 | backgroundColor[2]; // Malleus malificarum !
		int mColor = middleColor[0] << 16 | middleColor[1] << 8 | middleColor[2]; // Malleus malificarum !
		int fColor = foregroundColor[0] << 16 | foregroundColor[1] << 8 | foregroundColor[2]; // Malleus malificarum !
		for (int i = 0; i < height; ++i) {
			for (int j = 0; j < width; ++j) {
				// Récupération de la valeur de l'activateur
				double activator = patches[i][j].getActivator();

				if (activator * 3.0 > activationThreshold && activator < activationThreshold) {
					buffImage.setRGB(i, j, mColor);
				} else if (activator > activationThreshold) {
					buffImage.setRGB(i, j, fColor);
				} else {
					buffImage.setRGB(i, j, bColor);
				}
			}
		}
	}

	public Person mutate() {
		float newDiffusionRatio = this.diffusionRatio * mutationRatio();
		float newActivatorReactionRatio = this.activatorReactionRatio * mutationRatio();
		int newActivatorDiffusionSpeed = (int) (this.activatorDiffusionSpeed * mutationRatio());
		int newActivationThreshold = (int) (this.activationThreshold * mutationRatio());
		float newInhibitorReactionRatio = this.inhibitorReactionRatio * mutationRatio();
		int newInhibitorDiffusionSpeed = (int) (this.inhibitorDiffusionSpeed * mutationRatio());
		float newResorptionRatio = this.resorptionRatio * mutationRatio();

		for (int j = 0; j < 3; ++j) {
			backgroundColor[j] = (int) (this.backgroundColor[j] * mutationRatio());
			middleColor[j] = (int) (this.middleColor[j] * mutationRatio());
			foregroundColor[j] = (int) (this.foregroundColor[j] * mutationRatio());
		}
		int newMaxIterations = (int) (this.maxIterations * mutationRatio());

		Person mutant = new Person("mutated" + this.personName, this.height, this.width);

		mutant.setDiffusionRatio(newDiffusionRatio);

		mutant.setActivatorReactionRatio(newActivatorReactionRatio);
		mutant.setActivatorDiffusionSpeed(newActivatorDiffusionSpeed);
		
		mutant.setInhibitorReactionRatio(newInhibitorReactionRatio);
		mutant.setInhibitorDiffusionSpeed(newInhibitorDiffusionSpeed);

		mutant.setResorptionRatio(newResorptionRatio);
		
		mutant.setActivationThreshold(newActivationThreshold);
		
		mutant.setMaxIterations(newMaxIterations);
		
		mutant.createFur();

		return mutant;
	}

	public Person mutate(String personName) {
		float newDiffusionRatio = this.diffusionRatio * mutationRatio();
		float newActivatorReactionRatio = this.activatorReactionRatio * mutationRatio();
		int newActivatorDiffusionSpeed = (int) (this.activatorDiffusionSpeed * mutationRatio());
		int newActivationThreshold = (int) (this.activationThreshold * mutationRatio());
		float newInhibitorReactionRatio = this.inhibitorReactionRatio * mutationRatio();
		int newInhibitorDiffusionSpeed = (int) (this.inhibitorDiffusionSpeed * mutationRatio());
		float newResorptionRatio = this.resorptionRatio * mutationRatio();

		for (int j = 0; j < 3; ++j) {
			backgroundColor[j] = (int) (this.backgroundColor[j] * mutationRatio());
			middleColor[j] = (int) (this.middleColor[j] * mutationRatio());
			foregroundColor[j] = (int) (this.foregroundColor[j] * mutationRatio());
		}
		int newMaxIterations = (int) (this.maxIterations * mutationRatio());

		Person mutant = new Person(personName, this.height, this.width);

		mutant.setDiffusionRatio(newDiffusionRatio);

		mutant.setActivatorReactionRatio(newActivatorReactionRatio);
		mutant.setActivatorDiffusionSpeed(newActivatorDiffusionSpeed);

		mutant.setInhibitorReactionRatio(newInhibitorReactionRatio);
		mutant.setInhibitorDiffusionSpeed(newInhibitorDiffusionSpeed);

		mutant.setResorptionRatio(newResorptionRatio);

		mutant.setActivationThreshold(newActivationThreshold);

		mutant.setMaxIterations(newMaxIterations);

		mutant.createFur();

		return mutant;
	}
	// ===============
	
	// == Utilities ==
	public String toJson() {
		StringBuffer stringBuffer = new StringBuffer();

		stringBuffer.append("{\n");
		stringBuffer.append("\"name\": \"");
		stringBuffer.append(personName);
		stringBuffer.append("\",\n");
		stringBuffer.append("\"diffusionRatio\": ");
		stringBuffer.append(diffusionRatio);
		stringBuffer.append(",\n");
		stringBuffer.append("\"activatorReactionRatio\": ");
		stringBuffer.append(activatorReactionRatio);
		stringBuffer.append(",\n");
		stringBuffer.append("\"activatorDiffusionSpeed\": ");
		stringBuffer.append(activatorDiffusionSpeed);
		stringBuffer.append(",\n");
		stringBuffer.append("\"inhibitorReactionRatio\": ");
		stringBuffer.append(inhibitorReactionRatio);
		stringBuffer.append(",\n");
		stringBuffer.append("\"inhibitorDiffusionSpeed\": ");
		stringBuffer.append(inhibitorDiffusionSpeed);
		stringBuffer.append(",\n");
		stringBuffer.append("\"resorptionRatio\": ");
		stringBuffer.append(resorptionRatio);
		stringBuffer.append(",\n");
		stringBuffer.append("\"activationThreshold\":");
		stringBuffer.append(activationThreshold);
		stringBuffer.append(",\n");
		stringBuffer.append("\"maxIterations\":");
		stringBuffer.append(maxIterations);
		stringBuffer.append("\n}");
		return stringBuffer.toString();
	}
	
	private float mutationRatio() {
		return rnd.nextFloat() * (1.10f - 0.90f) + 0.90f;
	}
	// ===============
}
