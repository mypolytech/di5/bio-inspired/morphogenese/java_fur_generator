package fr.polytech.ybene.bio.launcher;

import fr.polytech.ybene.bio.Patch;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

public class FurGenerator {

	public static void main(String[] args) {
		// Taille de l'image finale
		int imageHeight = 800;
		int imageWidth = 800;

		// Iterations
		int maxIteration = 25;

		// Paramètres de génération
		double diffusionRatio = 0.15;
		double activatorReactionRatio = 0.5;
		int activatorDiffusionSpeed = 50;
		double inhibitorReactionRatio = 0.005;
		int inhibitorDiffusionSpeed = 5;
		double resorptionRatio = 0.001;
		int activationThreshold = 1000;

		// Liste des patchs
		Patch[][] patches = new Patch[imageHeight][imageWidth];
		// Création des patchs
		for (int i = 0; i < imageHeight; ++i) {
			for (int j = 0; j < imageWidth; ++j) {
				patches[i][j] = new Patch(i, j, imageHeight, imageWidth);
			}
		}

		// === ROUTINE ===
		for (int iterations = 0; iterations < maxIteration; ++iterations) {
			for (int i = 0; i < imageHeight; ++i) {
				for (int j = 0; j < imageWidth; ++j) {
					patches[i][j].react(activatorReactionRatio, inhibitorReactionRatio);
				}
			}

			// diffuse
			for (int i = 0; i < imageHeight; ++i) {
				for (int j = 0; j < imageWidth; ++j) {
					patches[i][j].diffuse(patches, diffusionRatio, activatorDiffusionSpeed, inhibitorDiffusionSpeed);
				}
			}

			// resorb
			for (int i = 0; i < imageHeight; ++i) {
				for (int j = 0; j < imageWidth; ++j) {
					patches[i][j].resorb(resorptionRatio);
				}
			}
		}
		// ===============

		// Création de l'image
		BufferedImage buffImage = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
		int white = 255 << 16 | 255 << 8 | 255; // Serait-ce de la magie noire ?
		int grey = 125 << 16 | 125 << 8 | 125; // Il semblerait... Malleus malificarum !

		for (int i = 0; i < imageHeight; ++i) {
			for (int j = 0; j < imageWidth; ++j) {
				// Récupération de la valeur de l'activateur
				double activator = patches[i][j].getActivator();

				if (activator < activationThreshold) {
					buffImage.setRGB(i, j, white);
				} else if (activator > activationThreshold && activator < activationThreshold * 2) {
					buffImage.setRGB(i, j, grey);
				} else {
					buffImage.setRGB(i, j, 0);
				}
			}
		}

		String path = folderChooser();
		try {

			File file = new File(path + "/generated_fun.png");
			ImageIO.write(buffImage, "png", file);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		System.out.println("Done...");
	}

	public static String folderChooser() {
		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle("choosertitle");
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		String path = "";
		//
		// disable the "All files" option.
		//
		chooser.setAcceptAllFileFilterUsed(false);
		//
		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			path = chooser.getSelectedFile().getAbsolutePath();
		} else {
			System.out.println("No Selection ");
		}
		return path;
	}
}
