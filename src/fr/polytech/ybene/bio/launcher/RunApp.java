package fr.polytech.ybene.bio.launcher;

import fr.polytech.ybene.bio.Person;
import fr.polytech.ybene.bio.controller.EnregistrerListener;
import fr.polytech.ybene.bio.controller.ModifierListener;
import fr.polytech.ybene.bio.controller.NewGenerationsListener;
import fr.polytech.ybene.bio.controller.Routine;
import fr.polytech.ybene.bio.ui.LoadingFrame;
import fr.polytech.ybene.bio.ui.MainFrame;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class RunApp extends JFrame implements PropertyChangeListener {

	// == Attributes ==
	private static final long serialVersionUID = 6034007655699732279L;
	public static final String LOOK_AND_FEEL = "javax.swing.plaf.nimbus.NimbusLookAndFeel";

	private List<Person> personList;
	private LoadingFrame loadingFrame;
	private MainFrame mainFrame;
	private JMenuBar jMenuBar;
	private JDialog jDialog;
	private int generationId;
	// ================

	// == Constructor ==
	public RunApp(String title) {

		super(title);

		personList = new ArrayList<>();
		mainFrame = new MainFrame();
		loadingFrame = new LoadingFrame();
		generationId = 1;

		this.setResizable(false);

		// PIMP MY APP
		URL iconURL = getClass().getResource("/fur-coat.png");
		ImageIcon icon = new ImageIcon(iconURL);
		setIconImage(icon.getImage());

		jDialog = new JDialog(this, "Veuillez patienter", true);
		jDialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
	}
	// =================

	// == Getters ==
	public List<Person> getPersonList() {
		return personList;
	}

	public LoadingFrame getLoadingFrame() {
		return loadingFrame;
	}

	public MainFrame getMainFrame() {
		return mainFrame;
	}
	// =============

	// == Setters ==
	public void setPersonList(List<Person> personList) {
		this.personList = personList;
	}

	public void setLoadingFrame(LoadingFrame loadingFrame) {
		this.loadingFrame = loadingFrame;
	}

	public void setMainFrame(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
	}
	
	public void setMenu() {
		jMenuBar = new JMenuBar();

		JMenu menu1 = new JMenu("Menu");
		JMenuItem enregistrer = new JMenuItem(new EnregistrerListener("Enregistrer", personList));
		enregistrer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_DOWN_MASK));
		menu1.add(enregistrer);

		jMenuBar.add(menu1);

		JMenu menu2 = new JMenu("?");
		jMenuBar.add(menu2);
		setJMenuBar(jMenuBar);
	}
	// =============

	// == Methods ==
	public void displayLoadingFrame() {
		setContentPane(loadingFrame.getMainPanel());
		setJMenuBar(null);
	}

	public void mainRun() {

		List<Person> selected = mainFrame.getSelected();
		// Execute la routine en tache de fond
		Routine routine = new Routine("G" + generationId, selected);
		routine.addPropertyChangeListener(this);
		routine.execute();

		loadingFrame.setProgressValue(0);

		// Creer la fenetre de chargement
		jDialog.setContentPane(loadingFrame.getMainPanel());
		jDialog.pack();
		jDialog.setLocationRelativeTo(this);
		jDialog.setVisible(true);
		jDialog.repaint();
		jDialog.revalidate();

		try {
			personList.clear();
			personList.addAll(routine.get());
			mainFrame.getDisplayImg0().setPerson(personList.get(0));
			mainFrame.getDisplayImg0().setModifyActionListener(new ModifierListener(this, personList.get(0)));

			mainFrame.getDisplayImg1().setPerson(personList.get(1));
			mainFrame.getDisplayImg1().setModifyActionListener(new ModifierListener(this, personList.get(1)));

			mainFrame.getDisplayImg2().setPerson(personList.get(2));
			mainFrame.getDisplayImg2().setModifyActionListener(new ModifierListener(this, personList.get(2)));

			mainFrame.getDisplayImg3().setPerson(personList.get(3));
			mainFrame.getDisplayImg3().setModifyActionListener(new ModifierListener(this, personList.get(3)));

			mainFrame.getDisplayImg4().setPerson(personList.get(4));
			mainFrame.getDisplayImg4().setModifyActionListener(new ModifierListener(this, personList.get(4)));

			mainFrame.getDisplayImg5().setPerson(personList.get(5));
			mainFrame.getDisplayImg5().setModifyActionListener(new ModifierListener(this, personList.get(5)));

			mainFrame.getDisplayImg6().setPerson(personList.get(6));
			mainFrame.getDisplayImg6().setModifyActionListener(new ModifierListener(this, personList.get(6)));

			mainFrame.getDisplayImg7().setPerson(personList.get(7));
			mainFrame.getDisplayImg7().setModifyActionListener(new ModifierListener(this, personList.get(7)));

			mainFrame.getDisplayImg8().setPerson(personList.get(8));
			mainFrame.getDisplayImg8().setModifyActionListener(new ModifierListener(this, personList.get(8)));

		} catch (InterruptedException ie) {
			System.out.println(ie.getMessage());
		} catch (ExecutionException ee) {
			System.out.println(ee.getMessage());
		}
		mainFrame.deselectAll();
		repaint();
		revalidate();

		generationId++;
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		
		if (evt.getPropertyName().equals("loading")) {
			int value = (Integer) evt.getNewValue();
			
			loadingFrame.setProgressValue(value);
			
			if (value == 8) {
				jDialog.setVisible(false);
			}
		}
	}
	// =============

	// == Main ==
	public static void main(String[] args) {
		try {
			// On change le look and feel en cours
			UIManager.setLookAndFeel(LOOK_AND_FEEL);
		} catch (Exception exception) {
			exception.printStackTrace();
			System.out.println(exception.getMessage());
		}

		RunApp runApp = new RunApp("PolyFur");

		LoadingFrame loadingFrame = runApp.getLoadingFrame();
		runApp.displayLoadingFrame();
		int loading = 0;
		loadingFrame.setProgressValue(loading);

		runApp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		runApp.pack();
		runApp.setVisible(true);

		// Taille de l'image finale
		int imageHeight = 200;
		int imageWidth = 200;

		// Creation de la premiere generation
		List<Person> personList = new ArrayList<>();

		for (int i = 0; i < 9; i++) {
			Person person = new Person("G0 - person " + i, imageHeight, imageWidth);
			person.setPosition(i);
			person.createFur();
			loadingFrame.setProgressValue(i);
			personList.add(person);
		}
		
		runApp.setPersonList(personList);

		MainFrame mainFrame = runApp.getMainFrame();
		// On charge les images dans la vue
		mainFrame.getDisplayImg0().setPerson(personList.get(0));

		mainFrame.getDisplayImg1().setPerson(personList.get(1));
		mainFrame.getDisplayImg2().setPerson(personList.get(2));
		mainFrame.getDisplayImg3().setPerson(personList.get(3));
		mainFrame.getDisplayImg4().setPerson(personList.get(4));
		mainFrame.getDisplayImg5().setPerson(personList.get(5));
		mainFrame.getDisplayImg6().setPerson(personList.get(6));
		mainFrame.getDisplayImg7().setPerson(personList.get(7));
		mainFrame.getDisplayImg8().setPerson(personList.get(8));

		mainFrame.getDisplayImg0().setModifyActionListener(new ModifierListener(runApp, personList.get(0)));
		mainFrame.getDisplayImg1().setModifyActionListener(new ModifierListener(runApp, personList.get(1)));
		mainFrame.getDisplayImg2().setModifyActionListener(new ModifierListener(runApp, personList.get(2)));
		mainFrame.getDisplayImg3().setModifyActionListener(new ModifierListener(runApp, personList.get(3)));
		mainFrame.getDisplayImg4().setModifyActionListener(new ModifierListener(runApp, personList.get(4)));
		mainFrame.getDisplayImg5().setModifyActionListener(new ModifierListener(runApp, personList.get(5)));
		mainFrame.getDisplayImg6().setModifyActionListener(new ModifierListener(runApp, personList.get(6)));
		mainFrame.getDisplayImg7().setModifyActionListener(new ModifierListener(runApp, personList.get(7)));
		mainFrame.getDisplayImg8().setModifyActionListener(new ModifierListener(runApp, personList.get(8)));

		NewGenerationsListener newGenerationsListener = new NewGenerationsListener(runApp);
		mainFrame.getButtonValidation().addActionListener(newGenerationsListener);

		runApp.setContentPane(mainFrame.getMainPanel());
		runApp.setMenu();

		runApp.pack();

	}
	// ==========
}
