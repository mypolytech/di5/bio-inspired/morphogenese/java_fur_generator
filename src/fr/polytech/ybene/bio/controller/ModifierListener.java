package fr.polytech.ybene.bio.controller;

import fr.polytech.ybene.bio.Person;
import fr.polytech.ybene.bio.ui.ParameterDisplay;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ModifierListener implements ActionListener {

	// == Attributes ==
	private JFrame jFrame;
	private Person person;
	// ================

	// == Constructor ==
	public ModifierListener(JFrame jFrame, Person person) {
		this.jFrame = jFrame;
		this.person = person;
	}
	// =================

	// == Methods ==
	@Override
	public void actionPerformed(ActionEvent e) {
		JDialog jDialog = new JDialog(jFrame, "Modifier " + person.getPersonName(), true);
		jDialog.setContentPane(new ParameterDisplay(person).getPanel1());
		jDialog.pack();
		jDialog.setLocationRelativeTo(jFrame);
		jDialog.setVisible(true);
		jDialog.repaint();
		jDialog.revalidate();
	}
	// =============
}
