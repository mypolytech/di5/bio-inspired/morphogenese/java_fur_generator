package fr.polytech.ybene.bio.controller;

import fr.polytech.ybene.bio.ColorPosition;
import fr.polytech.ybene.bio.Person;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class ColorListener implements ActionListener {

    // == Attributes ==
    private ColorPosition colorPosition;
    private Person person;
    private PropertyChangeSupport propertyChangeSupport;
    // ================

    // == Constructor ==
    public ColorListener(Person person, ColorPosition colorPosition) {
        this.person = person;
        this.colorPosition = colorPosition;
        propertyChangeSupport = new PropertyChangeSupport(this);
    }
    // =================

    // == Methods ==
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        propertyChangeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Color colorSet = null;

        switch (colorPosition) {
            case BACKGROUND:
                colorSet = new Color(person.getBackgroundColor()[0], person.getBackgroundColor()[1],
                        person.getBackgroundColor()[2]);
                break;
            case MIDDLE:
                colorSet = new Color(person.getMiddleColor()[0], person.getMiddleColor()[1], person.getMiddleColor()[2]);
                break;
            case FOREGROUND:
                colorSet = new Color(person.getForegroundColor()[0], person.getForegroundColor()[1],
                        person.getForegroundColor()[2]);
                break;
        }
        if (colorSet != null) {

            Color c = JColorChooser.showDialog(null, "Choose a Color", colorSet);
            if (c != null) {

                int[] colorPerson = new int[3];
                colorPerson[0] = c.getRed();
                colorPerson[1] = c.getGreen();
                colorPerson[2] = c.getBlue();

                String name = null;

                switch (colorPosition) {
                    case BACKGROUND:
                        person.setBackgroundColor(colorPerson);
                        name = "BACKGROUND";
                        break;
                    case MIDDLE:
                        person.setMiddleColor(colorPerson);
                        name = "MIDDLE";
                        break;
                    case FOREGROUND:
                        person.setForegroundColor(colorPerson);
                        name = "FOREGROUND";
                        break;
                }

                propertyChangeSupport.firePropertyChange(name, null, c);
            }
        }
    }
    // =============
}
