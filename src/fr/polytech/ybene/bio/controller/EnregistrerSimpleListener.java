package fr.polytech.ybene.bio.controller;

import fr.polytech.ybene.bio.Person;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class EnregistrerSimpleListener implements ActionListener {

	// == Attributes ==
	private Person person;
	private String path;
	private String typeImg;
	// ================

	// == Constructor ==
	public EnregistrerSimpleListener(Person person) {
		this.person = person;
	}
	// =================

	// == Methods ==
	@Override
	public void actionPerformed(ActionEvent e) {
		filePicker();
		try {

			File file = new File(path + "." + typeImg);
			ImageIO.write(person.getBuffImage(), typeImg, file);
		} catch (IOException ioException) {
			System.out.println(ioException.getMessage());
		} catch (IllegalArgumentException e1) {
			System.out.println(e1.getMessage());
		}

		try {
			File fileJson = new File(path + ".json");
			FileWriter fileWriter = new FileWriter(fileJson);
			if (fileJson.createNewFile()) {
				System.out.println("File created: " + fileJson.getName());
			} else {
				System.out.println("File already exists.");
			}

			fileWriter.write(person.toJson());
			fileWriter.close();
		} catch (IOException io) {
			System.out.println("An error occurred.");
			io.printStackTrace();
		}
	}

	private void filePicker() {
		JFileChooser chooser = new JFileChooser();

		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle("Enregistrer sous ...");

		FileNameExtensionFilter txtType = new FileNameExtensionFilter("png", "png");
		FileNameExtensionFilter htmlType = new FileNameExtensionFilter("jpg", "JPG");

		chooser.addChoosableFileFilter(txtType);
		chooser.addChoosableFileFilter(htmlType);

		path = "";

		// Disable the "All files" option.
		chooser.setAcceptAllFileFilterUsed(false);

		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			path = chooser.getSelectedFile().getAbsolutePath();
			FileFilter fileFilter = chooser.getFileFilter();
			typeImg = fileFilter.getDescription();
		} else {
			System.out.println("Pas de selection ");
		}
	}
	// =============
}
