package fr.polytech.ybene.bio.controller;

import fr.polytech.ybene.bio.Person;
import fr.polytech.ybene.bio.ui.PopUpMenu;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ImageClickedListener implements MouseListener {

	// == Attributes ==
	private JLabel img;
	private JCheckBox checkBox;
	private Person person;
	// ================

	// == Constructor ==
	public ImageClickedListener(JLabel jLabel, JCheckBox jCheckBox) {
		img = jLabel;
		checkBox = jCheckBox;
	}
	// =================

	// == Methods ==
	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		if (e.getButton() == MouseEvent.BUTTON1) {
			checkBox.setSelected(!checkBox.isSelected());
		} else if (e.getButton() == MouseEvent.BUTTON3) {
			PopUpMenu popUpMenu = new PopUpMenu(person);
			popUpMenu.show(img, e.getX(), e.getY());
			;
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// Empty
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// Empty
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// Empty
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// Empty
	}
	// =============
}
