package fr.polytech.ybene.bio.controller;

import fr.polytech.ybene.bio.Person;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class EnregistrerListener extends AbstractAction {

	// == Attributes ==
	private static final long serialVersionUID = -4382886069947844823L;
	private List<Person> personList;
	// ================

	// == Constructor ==
	public EnregistrerListener(String action, List<Person> personList) {
		super(action);
		this.personList = personList;
	}
	// =================

	// == Methods ==
	@Override
	public void actionPerformed(ActionEvent e) {

		String path = folderChooser();

		for (Person person : personList) {
			try {

				File file = new File(path + File.pathSeparator + person.getPersonName() + ".png");
				ImageIO.write(person.getBuffImage(), "png", file);
			} catch (IOException ioException) {
				System.out.println(ioException.getMessage());
			}

			File fileJson = new File(path + File.pathSeparator + person.getPersonName() + ".json");

			try (FileWriter fileWriter = new FileWriter(fileJson)) {

				if (fileJson.createNewFile()) {
					System.out.println("File created: " + fileJson.getName());
				} else {
					System.out.println("File already exists.");
				}

				fileWriter.write(person.toJson());
			} catch (IOException io) {
				System.out.println("An error occurred.");
				io.printStackTrace();
			}
		}
	}

	public String folderChooser() {
		JFileChooser chooser = new JFileChooser();

		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle("Enregistrer sous ...");
		chooser.setApproveButtonText("Save");
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		String path = "";

		// Disable the "All files" option.
		chooser.setAcceptAllFileFilterUsed(false);

		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			path = chooser.getSelectedFile().getAbsolutePath();
		} else {
			System.out.println("Pas de selection ");
		}

		return path;
	}
	// =============
}