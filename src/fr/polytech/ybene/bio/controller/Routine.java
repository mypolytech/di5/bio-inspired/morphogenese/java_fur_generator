package fr.polytech.ybene.bio.controller;

import fr.polytech.ybene.bio.Crossover;
import fr.polytech.ybene.bio.Person;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Routine extends SwingWorker<List<Person>, Integer> {

	// == Attributes ==
	private String generation;
	private List<Person> personList;
	private Random rnd = new Random();
	// ================

	// == Constructor ==
	public Routine(String generation, List<Person> personList) {
		this.generation = generation;
		this.personList = personList;
	}
	// =================

	// == Methods ==
	@Override
	protected List<Person> doInBackground() {
		// Simulate doing something useful.
		int imageHeight = 200;
		int imageWidth = 200;

		// Nouvelle génération
		List<Person> newGen = new ArrayList<>();

		// Récupération du nombre de parents sélectionnés
		int sizeList = personList.size();

		// Début de la génération des nouveaux individus
		if (sizeList == 0) {
			// Aucun parent n'est sélectionné
			// On génère de nouveaux individus aléatoirement
			for (int i = 0; i < 9; i++) {
				Person person = new Person(generation + "- person " + i, imageHeight, imageWidth);
				person.setPosition(i);
				person.createFur();
				getPropertyChangeSupport().firePropertyChange("loading", null, i);
				newGen.add(person);
			}
		} else if (sizeList == 1) {
			// Un seul individu sélectionné
			// Les enfants sont des mutations de cet individu
			Person father = personList.get(0);

			for (int i = 0; i < 9; i++) {
				if (i != father.getPosition()) {
					Person son = father.mutate(generation + "- person " + i);
					son.setPosition(i);
					newGen.add(son);
				} else {
					father.setPosition(i);
					newGen.add(father);
					father.recolor();
				}

				getPropertyChangeSupport().firePropertyChange("loading", null, i);
			}
		} else {
			// Plusieurs parent sélectionnés
			// On fait des croisements
			for (int i = 0; i < 9; i++) {

				boolean skip = false;

				for (Person pers : personList) {
					pers.recolor();
					if (i == pers.getPosition()) {
						newGen.add(pers);
						skip = true;
					}
				}

				if (!skip) {
					// Tirage de deux parents au hasard (avec remise)
					int first = rnd.nextInt(sizeList);
					int second = rnd.nextInt(sizeList);
					// On s'assure que les deux parents sont différents
					while (first == second) {
						second = rnd.nextInt(sizeList);
					}

					// On récupère les parents
					Person firstParent = personList.get(first);
					Person secondParent = personList.get(second);

					// On créée l'enfant
					Person breed = Crossover.mutateOrBreed(firstParent, secondParent, generation + "- person " + i);
					breed.setPosition(i);
					newGen.add(breed);
				}

				getPropertyChangeSupport().firePropertyChange("loading", null, i);
			}
		}

		newGen.sort((p1, p2) -> {
			return p1.getPosition() >= p2.getPosition() ? 1 : 0;
		});

		return newGen;
	}
	// =============
}
