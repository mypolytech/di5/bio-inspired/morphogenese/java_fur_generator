package fr.polytech.ybene.bio.controller;

import fr.polytech.ybene.bio.launcher.RunApp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NewGenerationsListener implements ActionListener {

	// == Attributes ==
	private RunApp runApp;
	// ================

	// == Constructor ==
	public NewGenerationsListener(RunApp runApp) {
		this.runApp = runApp;
	}
	// =================

	// == Methods ==
	@Override
	public void actionPerformed(ActionEvent e) {
		runApp.mainRun();
	}
	// =============
}
