package fr.polytech.ybene.bio;

public enum ColorPosition {
	BACKGROUND, MIDDLE, FOREGROUND;
}
