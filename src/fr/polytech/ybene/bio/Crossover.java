package fr.polytech.ybene.bio;

import java.util.Random;

public class Crossover {
	
	// == Attributes ==
	private static Random rnd = new Random();
	// ================
	
	// == Constructors ==
	private Crossover() {}
	// ==================
	
	// == Methods ==
	public static Person mutateOrBreed(Person firstParent, Person secondParent) {
		
		int mutateOrBreed = rnd.nextInt(3);
		Person descendent;
		
		if(mutateOrBreed == 0) {
			descendent = new Person("breed", firstParent.getHeight(), firstParent.getWidth());
			breed(firstParent, secondParent, descendent);
			descendent.createFur();
			return descendent;
		} else if (mutateOrBreed == 1) {
			descendent = firstParent.mutate();
			descendent.createFur();
			return descendent;
		} else if (mutateOrBreed == 2) {
			descendent = secondParent.mutate();
			descendent.createFur();
			return descendent;
		} else {			
			return null;
		}
	}
	public static Person mutateOrBreed(Person firstParent, Person secondParent, String personName) {

		int mutateOrBreed = rnd.nextInt(3);
		Person descendent;

		if(mutateOrBreed == 0) {
			descendent = new Person(personName, firstParent.getHeight(), firstParent.getWidth());
			breed(firstParent, secondParent, descendent);
			descendent.createFur();
			return descendent;
		} else if (mutateOrBreed == 1) {
			descendent = firstParent.mutate();
			descendent.createFur();
			return descendent;
		} else if (mutateOrBreed == 2) {
			descendent = secondParent.mutate();
			descendent.createFur();
			return descendent;
		} else {
			return null;
		}
	}

	private static void breed(Person firstParent, Person secondParent, Person descendent) {
		float crossDiffusionRatio = chooseOne(firstParent, secondParent).getDiffusionRatio();
		descendent.setDiffusionRatio(crossDiffusionRatio);
		
		float crossActiReacRatio = chooseOne(firstParent, secondParent).getActivatorReactionRatio();
		descendent.setActivatorReactionRatio(crossActiReacRatio);
		int crossActiDiffSpeed = chooseOne(firstParent, secondParent).getActivatorDiffusionSpeed();
		descendent.setActivatorDiffusionSpeed(crossActiDiffSpeed);
		
		float crossInhibReacRatio = chooseOne(firstParent, secondParent).getInhibitorReactionRatio();
		descendent.setInhibitorReactionRatio(crossInhibReacRatio);
		int crossInhibDiffSpeed = chooseOne(firstParent, secondParent).getActivatorDiffusionSpeed();
		descendent.setInhibitorDiffusionSpeed(crossInhibDiffSpeed);
		
		float crossResorpRatio = chooseOne(firstParent, secondParent).getResorptionRatio();
		descendent.setResorptionRatio(crossResorpRatio);
		
		int crossActiThreshold = chooseOne(firstParent, secondParent).getActivationThreshold();
		descendent.setActivationThreshold(crossActiThreshold);
		
		int crossMaxIte = chooseOne(firstParent, secondParent).getMaxIterations();
		descendent.setMaxIterations(crossMaxIte);
	}
	// =============

	// == Utilities ==
	private static Person chooseOne(Person firstParent, Person secondParent) {
		return (rnd.nextInt(2) + 1) == 1 ? firstParent : secondParent;
	}
	// ===============
}
