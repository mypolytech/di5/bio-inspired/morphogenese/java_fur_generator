package fr.polytech.ybene.bio.ui;

import fr.polytech.ybene.bio.Person;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class MainFrame {
	
	// == Attributes ==
    private JButton ButtonValidation;
    private DisplayImg DisplayImg0;
    private DisplayImg DisplayImg1;
    private DisplayImg DisplayImg2;
    private DisplayImg DisplayImg3;
    private DisplayImg DisplayImg4;
    private DisplayImg DisplayImg5;
    private DisplayImg DisplayImg6;
    private DisplayImg DisplayImg7;
    private DisplayImg DisplayImg8;
    private JPanel MainPanel;
	// ================

	// == Getters ==
    public JButton getButtonValidation() {
        return ButtonValidation;
    }
    
    public DisplayImg getDisplayImg0() {
        return DisplayImg0;
    }
    
    public DisplayImg getDisplayImg1() {
    	return DisplayImg1;
    }
    
    public DisplayImg getDisplayImg2() {
    	return DisplayImg2;
    }
    
    public DisplayImg getDisplayImg3() {
    	return DisplayImg3;
    }
    
    public DisplayImg getDisplayImg4() {
    	return DisplayImg4;
    }
    
    public DisplayImg getDisplayImg5() {
    	return DisplayImg5;
    }

    public DisplayImg getDisplayImg6() {
        return DisplayImg6;
    }

    public DisplayImg getDisplayImg7() {
        return DisplayImg7;
    }
    
    public DisplayImg getDisplayImg8() {
        return DisplayImg8;
    }

    public JPanel getMainPanel() {
        return MainPanel;
    }
	// =============

	// == Setters ==
    public void setButtonValidation(JButton buttonValidation) {
        ButtonValidation = buttonValidation;
    }
    
    public void setDisplayImg0(DisplayImg displayImg0) {
        DisplayImg0 = displayImg0;
    }

    public void setDisplayImg1(DisplayImg displayImg1) {
        DisplayImg1 = displayImg1;
    }

    public void setDisplayImg2(DisplayImg displayImg2) {
        DisplayImg2 = displayImg2;
    }

    public void setDisplayImg3(DisplayImg displayImg3) {
        DisplayImg3 = displayImg3;
    }

    public void setDisplayImg4(DisplayImg displayImg4) {
        DisplayImg4 = displayImg4;
    }

    public void setDisplayImg5(DisplayImg displayImg5) {
        DisplayImg5 = displayImg5;
    }

    public void setDisplayImg6(DisplayImg displayImg6) {
        DisplayImg6 = displayImg6;
    }

    public void setDisplayImg7(DisplayImg displayImg7) {
        DisplayImg7 = displayImg7;
    }

    public void setDisplayImg8(DisplayImg displayImg8) {
        DisplayImg8 = displayImg8;
    }

    public void setMainPanel(JPanel mainPanel) {
        MainPanel = mainPanel;
    }
	// =============

	// == Methods ==
    public List<Person> getSelected() {
        List<Person> personList = new ArrayList<>();
        if (DisplayImg0.isSelected()) {
            personList.add(getDisplayImg0().getPerson());
        }
        if (DisplayImg1.isSelected()) {
            personList.add(getDisplayImg1().getPerson());
        }
        if (DisplayImg2.isSelected()) {
            personList.add(getDisplayImg2().getPerson());
        }
        if (DisplayImg3.isSelected()) {
            personList.add(getDisplayImg3().getPerson());
        }
        if (DisplayImg4.isSelected()) {
            personList.add(getDisplayImg4().getPerson());
        }
        if (DisplayImg5.isSelected()) {
            personList.add(getDisplayImg5().getPerson());
        }
        if (DisplayImg6.isSelected()) {
            personList.add(getDisplayImg6().getPerson());
        }
        if (DisplayImg7.isSelected()) {
            personList.add(getDisplayImg7().getPerson());
        }
        if (DisplayImg8.isSelected()) {
            personList.add(getDisplayImg8().getPerson());
        }

        return personList;
    }
    
    public void deselectAll() {
        DisplayImg0.deselect();
        DisplayImg1.deselect();
        DisplayImg2.deselect();
        DisplayImg3.deselect();
        DisplayImg4.deselect();
        DisplayImg5.deselect();
        DisplayImg6.deselect();
        DisplayImg7.deselect();
        DisplayImg8.deselect();
    }
	// =============

    // == Black magic ==
    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        MainPanel = new JPanel();
        MainPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(4, 3, new Insets(10, 10, 10, 10), 10, 10));
        DisplayImg0 = new DisplayImg();
        MainPanel.add(DisplayImg0.$$$getRootComponent$$$(), new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        DisplayImg1 = new DisplayImg();
        MainPanel.add(DisplayImg1.$$$getRootComponent$$$(), new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        DisplayImg2 = new DisplayImg();
        MainPanel.add(DisplayImg2.$$$getRootComponent$$$(), new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        DisplayImg3 = new DisplayImg();
        MainPanel.add(DisplayImg3.$$$getRootComponent$$$(), new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        DisplayImg4 = new DisplayImg();
        MainPanel.add(DisplayImg4.$$$getRootComponent$$$(), new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        DisplayImg5 = new DisplayImg();
        MainPanel.add(DisplayImg5.$$$getRootComponent$$$(), new com.intellij.uiDesigner.core.GridConstraints(1, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        DisplayImg6 = new DisplayImg();
        MainPanel.add(DisplayImg6.$$$getRootComponent$$$(), new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        DisplayImg7 = new DisplayImg();
        MainPanel.add(DisplayImg7.$$$getRootComponent$$$(), new com.intellij.uiDesigner.core.GridConstraints(2, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        DisplayImg8 = new DisplayImg();
        MainPanel.add(DisplayImg8.$$$getRootComponent$$$(), new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        ButtonValidation = new JButton();
        ButtonValidation.setEnabled(true);
        ButtonValidation.setMargin(new Insets(0, 0, 0, 0));
        ButtonValidation.setText("Génération suivante");
        MainPanel.add(ButtonValidation, new com.intellij.uiDesigner.core.GridConstraints(3, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return MainPanel;
    }
    // =================
}
