package fr.polytech.ybene.bio.ui;

import fr.polytech.ybene.bio.Person;
import fr.polytech.ybene.bio.controller.EnregistrerSimpleListener;

import javax.swing.*;

public class PopUpMenu extends JPopupMenu {
	
	// == Attributes ==
	private static final long serialVersionUID = -876198670705272039L;
	JMenuItem saveAs;
	// ================

	// == Methods ==
	public PopUpMenu(Person person){
		saveAs = new JMenuItem("Enregistrer sous ...");
		EnregistrerSimpleListener enregistrerSimpleListener = new EnregistrerSimpleListener(person);
		saveAs.addActionListener(enregistrerSimpleListener);
		add(saveAs);
	}
	// =============
}
