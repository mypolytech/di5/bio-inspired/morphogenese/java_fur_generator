package fr.polytech.ybene.bio;

import java.util.Random;

public class Patch {

	// === Attributs ===
	int activator;
	int inhibitor;
	int posX;
	int posY;
	int maxX;
	int maxY;
	// =================

	// === Constructeur ===
	public Patch(int x, int y, int maxX, int maxY) {
		// Création de l'outil d'aléatoire
		Random rnd = new Random();

		this.posX = x;
		this.posY = y;
		this.maxX = maxX;
		this.maxY = maxY;

		this.activator = rnd.nextInt(100) + 1;
		this.inhibitor = rnd.nextInt(100) + 1;
	}
	// ====================

	// === Getters ===
	public double getActivator() {
		return activator;
	}

	public double getInhibitor() {
		return inhibitor;
	}

	public int getPosX() {
		return posX;
	}

	public int getPosY() {
		return posY;
	}
	// ===============

	// === ADD ===
	private void addActivator(double actValue) {
		this.activator += actValue;
	}

	private void addInhibitor(double inhibValue) {
		this.inhibitor += inhibValue;
	}
	// ===========

	// === REACTION ===
	/**
	 * Implémentation de la réaction
	 */
	public void react(double activatorReactionRatio, double inhibitorReactionRatio) {
		// On récupère la valeur de l'inhibiteur avant de la modifier
		double oldInhibitor = this.inhibitor;

		// Calcul de la nouvelle valeur de l'inhibiteur
		this.inhibitor += inhibitorReactionRatio * Math.pow(activator, 2);

		// Calcul de la nouvelle valeur de l'activateur
		this.activator += (activatorReactionRatio * Math.pow(activator, 2)) / oldInhibitor;
	}
	// ================

	// === DIFFUSION ===
	/**
	 * Implémentation de la diffusion
	 */
	public void diffuse(Patch[][] patches, double diffusionRatio, int actDiffSpd, int inhibDiffSpd) {
		// Diffusion de l'activateur
		for (int i = 0; i < actDiffSpd; ++i) {
			// La valeur de l'activateur à donner aux voisins
			double actShared = this.activator * diffusionRatio;

			// On partage avec les voisins
			shareActivator(patches, actShared / 8);

			// On met à jour la valeur pour la cellule actuelle
			this.activator -= actShared;
		}

		// Diffusion de l'inhibiteur
		for (int i = 0; i < inhibDiffSpd; ++i) {
			// La valeur de l'inhibiteur à donner aux voisins
			double inhibShared = this.inhibitor * diffusionRatio;

			// On partage avec les voisins
			shareInhibitor(patches, inhibShared / 8);

			// On met à jour la valeur pour la cellule actuelle
			this.inhibitor -= inhibShared;
		}
	}

	/**
	 * Partage de l'activateur avec les voisins
	 */
	private void shareActivator(Patch[][] patches, double actShared) {
		// Ligne du dessus
		if (posX - 1 >= 0) {
			if (posY - 1 >= 0) {
				patches[posX - 1][posY - 1].addActivator(actShared);
			}
			if (posY + 1 < maxY) {
				patches[posX - 1][posY + 1].addActivator(actShared);
			}

			patches[posX - 1][posY].addActivator(actShared);
		}

		// Ligne du milieu
		if (posY - 1 >= 0) {
			patches[posX][posY - 1].addActivator(actShared);
		}
		if (posY + 1 < maxY) {
			patches[posX][posY + 1].addActivator(actShared);
		}

		// Ligne du dessous
		if (posX + 1 < maxX) {
			if (posY - 1 >= 0) {
				patches[posX + 1][posY - 1].addActivator(actShared);
			}
			if (posY + 1 < maxY) {
				patches[posX + 1][posY + 1].addActivator(actShared);
			}

			patches[posX + 1][posY].addActivator(actShared);
		}
	}

	/**
	 * Partage de l'inhibiteur avec les voisins
	 */
	private void shareInhibitor(Patch[][] patches, double inhibShared) {
		// Ligne du dessus
		if (posX - 1 >= 0) {
			if (posY - 1 >= 0) {
				patches[posX - 1][posY - 1].addInhibitor(inhibShared);
			}
			if (posY + 1 < maxY) {
				patches[posX - 1][posY + 1].addInhibitor(inhibShared);
			}

			patches[posX - 1][posY].addInhibitor(inhibShared);
		}

		// Ligne du milieu
		if (posY - 1 >= 0) {
			patches[posX][posY - 1].addInhibitor(inhibShared);
		}
		if (posY + 1 < maxY) {
			patches[posX][posY + 1].addInhibitor(inhibShared);
		}

		// Ligne du dessous
		if (posX + 1 < maxX) {
			if (posY - 1 >= 0) {
				patches[posX + 1][posY - 1].addInhibitor(inhibShared);
			}
			if (posY + 1 < maxY) {
				patches[posX + 1][posY + 1].addInhibitor(inhibShared);
			}

			patches[posX + 1][posY].addInhibitor(inhibShared);
		}
	}
	// =================

	// === RESORPTION ===
	/**
	 * Implémentation de la résorption
	 */
	public void resorb(double resorptionRatio) {
		this.activator *= (1 - resorptionRatio);
		this.inhibitor *= (1 - resorptionRatio);
	}
	// ==================
}
