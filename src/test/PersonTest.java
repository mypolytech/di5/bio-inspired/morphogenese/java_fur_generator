package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.polytech.ybene.bio.Person;

class PersonTest {

	private Person person;

	@BeforeEach
	private void setup() {
		person = new Person("test_dummy", 0, 0);
	}
	
	@Test
	void diffusionRatiotest() {
		assertEquals(true, person.getDiffusionRatio() <= 1.0 && person.getDiffusionRatio() >= 0.0);
	}
	
	@Test
	void activatorReactionRatioTest() {
		assertEquals(true, person.getActivatorReactionRatio() <= 1.0 && person.getActivatorReactionRatio() >= 0.0);
	}
	
	@Test
	void activatorDiffusionSpeedTest() {
		assertEquals(true, person.getActivatorDiffusionSpeed() <= 100 && person.getActivatorDiffusionSpeed() >= 1);
	}
	
	@Test 
	void inhibitorReactionRatioTest() {
		assertEquals(true, person.getInhibitorReactionRatio() <= 1.0 && person.getInhibitorReactionRatio() >= 0.0);
	}
	
	@Test
	void inhibitorDiffusionSpeedTest() {
		assertEquals(true, person.getInhibitorDiffusionSpeed() <= 100 && person.getInhibitorDiffusionSpeed() >= 1);
	}
	
	@Test
	void resorptionRatioTest() {
		assertEquals(true, person.getResorptionRatio() <= 1.0 && person.getResorptionRatio() >= 0.0);
	}
	
	@Test
	void activationThresholdTest() {
		assertEquals(true, person.getActivationThreshold() <= 70 && person.getActivationThreshold() >= 1);
	}
	
	@Test
	void maxIterationsTest() {
		assertEquals(true, person.getMaxIterations() <= 10 && person.getMaxIterations() >= 1);
	}
}
